#!/bin/bash

alacritty_config_dir=~/.config/alacritty
is_dark=`grep -c 'dark' "$alacritty_config_dir/theme.toml"`

if [[ $is_dark > 0 ]]; then
  lookandfeeltool -a org.kde.breeze.desktop
  sed -i 's/dark/light/' ~/.config/alacritty/theme.toml
  sed -i 's/Catppuccin-macchiato/dayfox/' ~/.config/bat/config
  light.sh
else
  lookandfeeltool -a org.kde.breezedark.desktop
  sed -i 's/light/dark/' ~/.config/alacritty/theme.toml
  sed -i 's/dayfox/Catppuccin-macchiato/' ~/.config/bat/config
  dark.sh
fi

touch ~/.config/alacritty/alacritty.toml
