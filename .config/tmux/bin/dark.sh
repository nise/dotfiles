thm_bg="#24273a"
thm_fg="#cad3f5"
thm_cyan="#91d7e3"
thm_black="#1e2030"
thm_gray="#363a4f"
thm_magenta="#c6a0f6"
thm_pink="#f5bde6"
thm_red="#ed8796"
thm_green="#a6da95"
thm_yellow="#eed49f"
thm_blue="#8aadf4"
thm_orange="#f5a97f"
thm_black4="#5b6078"

tmux set-option -g status-bg $thm_black
tmux set-option -g status-fg $thm_fg
tmux set -g window-status-format "$(update-window-theme.sh $thm_blue $thm_gray $thm_black $thm_fg $thm_bg)"
tmux set -g window-status-current-format "$(update-window-theme.sh $thm_orange $thm_bg $thm_gray $thm_orange $thm_bg)"
update-status-theme.sh $thm_black $thm_orange $thm_black $thm_gray
