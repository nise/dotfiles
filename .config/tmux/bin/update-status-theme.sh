tmux set -g @cpu_low_bg_color "#[fg=green, bg=$1]"
tmux set -g @cpu_medium_bg_color "#[fg=yellow, bg=$1]"
tmux set -g @cpu_high_bg_color "#[fg=red, bg=$1]"

window_right_separator="  "
show_right_separator="#[fg=$2,bg=$3,nobold,nounderscore,noitalics]$window_right_separator"
tmux set-window-option -g status-left "#[fg=$4,bg=$2]  #S $show_right_separator"
