thm_bg="#f6f2ee"
thm_fg="#3d2b5a"
thm_black="#352c24"
thm_red="#a5222f"
thm_green="#396847"
thm_yellow="#AC5402"
thm_blue="#2848a9"
thm_magenta="#6e33ce"
thm_cyan="#287980"
thm_white="#f2e9e1"
thm_orange="#955f61"
thm_pink="#a440b5"
thm_gray="#e4dcd4"
thm_black4="#5b6078"

tmux set-option -g status-bg $thm_white
tmux set-option -g status-fg $thm_fg
tmux set -g window-status-format "$(update-window-theme.sh $thm_blue $thm_gray $thm_white $thm_fg $thm_bg)"
tmux set -g window-status-current-format "$(update-window-theme.sh $thm_yellow $thm_bg $thm_gray $thm_yellow $thm_bg)"
update-status-theme.sh $thm_white $thm_yellow $thm_white $thm_gray
