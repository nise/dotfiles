local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  use 'nvim-lua/plenary.nvim'
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use {
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons', -- optional
    },
    config = function()
      require("nvim-tree").setup {}
    end
  }

  use { 'nvim-telescope/telescope.nvim',
    requires = {
      { "nvim-telescope/telescope-live-grep-args.nvim" },
    },
    branch = '0.1.x' }
  use 'nvim-telescope/telescope-ui-select.nvim'
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
  use 'kyazdani42/nvim-web-devicons'
  use { 'ThePrimeagen/harpoon', branch = 'harpoon2' }
  use { 'm00qek/baleia.nvim', tag = 'v1.4.0' }

  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }
  use 'windwp/nvim-autopairs'
  use 'windwp/nvim-ts-autotag'

  use {
    'lewis6991/gitsigns.nvim',
    config = function()
      require('gitsigns').setup()
    end
  }
  use 'folke/which-key.nvim'
  use 'norcalli/nvim-colorizer.lua'
  use 'mbbill/undotree'
  use "folke/zen-mode.nvim"
  use {
    "luukvbaal/nnn.nvim",
    config = function() require("nnn").setup() end
  }
  use "RRethy/vim-illuminate"

  use 'neovim/nvim-lspconfig'
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use 'nvimtools/none-ls.nvim'
  use {
    "nvim-neotest/neotest",
    requires = {
      "nvim-lua/plenary.nvim",
      "antoinemadec/FixCursorHold.nvim",
      "nvim-treesitter/nvim-treesitter"
    }
  }
  use 'lvimuser/lsp-inlayhints.nvim'

  use({
    "nvim-treesitter/nvim-treesitter-textobjects",
    after = "nvim-treesitter",
    requires = "nvim-treesitter/nvim-treesitter",
  })
  use 'j-hui/fidget.nvim'

  -- Code completion
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-cmdline'
  use 'saadparwaiz1/cmp_luasnip'
  use 'hrsh7th/cmp-nvim-lua'
  use 'hrsh7th/cmp-nvim-lsp-signature-help'
  use 'rcarriga/cmp-dap'

  -- Snippet engine
  use({ "L3MON4D3/LuaSnip", tag = "v2.*" })
  use "rafamadriz/friendly-snippets"

  -- Adds extra functionality over rust analyzer
  use 'mrcjkb/rustaceanvim'

  use "IndianBoy42/tree-sitter-just"
  use "MTDL9/vim-log-highlighting"

  use "folke/neodev.nvim"

  use "lukas-reineke/indent-blankline.nvim"
  -- Some color schemes other than default
  use 'arcticicestudio/nord-vim'
  use 'ayu-theme/ayu-vim'
  use 'nlknguyen/papercolor-theme'
  use 'ishan9299/nvim-solarized-lua'
  use 'navarasu/onedark.nvim'
  use 'tjdevries/colorbuddy.nvim'
  use 'svrana/neosolarized.nvim'
  use 'folke/tokyonight.nvim'
  use { 'rose-pine/neovim', as = 'rose-pine' }
  use "rebelot/kanagawa.nvim"
  use {
    "mcchrish/zenbones.nvim",
    -- Optionally install Lush. Allows for more configuration or extending the colorscheme
    -- If you don't want to install lush, make sure to set g:zenbones_compat = 1
    -- In Vim, compat mode is turned on as Lush only works in Neovim.
    requires = "rktjmp/lush.nvim"
  }
  use "EdenEast/nightfox.nvim" -- Packer
  use { "catppuccin/nvim", as = "catppuccin" }

  -- java
  use 'mfussenegger/nvim-jdtls'
  use 'mfussenegger/nvim-dap'
  use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" } }

  -- python
  use "nvim-neotest/neotest-python"
  use 'mfussenegger/nvim-dap-python'

  use 'sile-typesetter/vim-sile'
  use {
    'pwntester/octo.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim',
      -- OR 'ibhagwan/fzf-lua',
      'nvim-tree/nvim-web-devicons',
    },
    config = function()
      require "octo".setup()
    end
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
