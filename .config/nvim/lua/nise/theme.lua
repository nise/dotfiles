local M = {}

local catppuccin_palette = require('catppuccin.palettes')

local home = os.getenv("HOME")
local theme_file_path = home .. "/.config/alacritty/theme.toml"

local function switch_to_theme(theme_name, indent_theme_builder)
  local hooks = require "ibl.hooks"
  hooks.clear_all()
  hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
    local indent_theme = indent_theme_builder()
    vim.api.nvim_set_hl(0, "IblScope", { fg = indent_theme.scope_color })
    vim.api.nvim_set_hl(0, "IblWhitespace", { fg = indent_theme.whitespace_color })
  end)

  require("ibl").setup { indent = { char = '│' } }

  vim.cmd.colorscheme(theme_name)

  require 'lualine'.setup {
    options = {
      theme = theme_name
    },
    sections = {
      lualine_b = {
        'branch',
        'diagnostics',
        'diff',
      },
      lualine_c = {
        {
          'filename',
          path = 1
        }
      }
    },
    extensions = { 'nvim-tree', 'man', 'mason', 'nvim-dap-ui' }
  }
end

local function init_catppuccin()
  vim.opt.bg = "dark"
  switch_to_theme('catppuccin-macchiato', function()
    local palette = catppuccin_palette.get_palette()
    return {
      scope_color = palette.subtext0,
      whitespace_color = palette.surface0
    }
  end)
end

local function init_dayfox()
  switch_to_theme('dayfox', function()
    local palette = require('nightfox.palette.dayfox').palette
    return {
      scope_color = palette.fg1,
      whitespace_color = palette.bg2
    }
  end)
end

function M.toggle()
  if vim.g.colors_name == 'catppuccin-macchiato' then
    init_dayfox()
  else
    init_catppuccin()
  end
end

function theme_is_light()
  local file = io.open(theme_file_path, "r")
  return file and string.find(file:read(), "light")
end

function M.setup()
  vim.opt.list = true
  vim.opt.listchars:append "space:·,trail: "
  if theme_is_light() then
    init_dayfox()
  else
    init_catppuccin()
  end
end

return M
