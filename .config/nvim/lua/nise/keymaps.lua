local keymap = vim.keymap.set;

local function normal(custom, standard)
  keymap('n', custom, standard)
end

local function insert(custom, standard)
  keymap('i', custom, standard)
end

local function visual(custom, standard)
  keymap('v', custom, standard)
end

local function visual_block(custom, standard)
  keymap('x', custom, standard)
end

local function n_command(key, command, description)
  keymap('n', key, command, { desc = description })
end

vim.g.mapleader = " ";
vim.g.maplocalleader = " ";

normal('<m-h>', '<C-w>h');
normal('<m-j>', '<C-w>j');
normal('<m-k>', '<C-w>k');
normal('<m-l>', '<C-w>l');
-- bépo bindings
normal('<m-c>', '<C-w>h');
normal('<m-t>', '<C-w>j');
normal('<m-s>', '<C-w>k');
normal('<m-r>', '<C-w>l');

normal('<C-Up>', '<Cmd>resize +2<CR>');
normal('<C-Down>', '<Cmd>resize -2<CR>');
normal('<C-Left>', '<Cmd>vertical resize -2<CR>');
normal('<C-Right>', '<Cmd>vertical resize +2<CR>');

normal('<C-u>', '<C-u>zz')
normal('<C-t>', '<C-u>zz')
normal('<C-d>', '<C-d>zz')
normal('n', 'nzz')
normal('N', 'Nzz')

normal('<C-j>', ':m .+1<CR>==')
normal('<C-k>', ':m .-2<CR>==')
visual_block('<C-j>', ":m '>+1<CR>gv=gv")
visual_block('<C-k>', ":m '<-2<CR>gv=gv")
normal('<C-q>', ':m .+1<CR>==')
normal('<C-g>', ':m .-2<CR>==')
visual_block('<C-q>', ":m '>+1<CR>gv=gv")
visual_block('<C-g>', ":m '<-2<CR>gv=gv")

insert('jk', '<ESC>');
visual('jk', '<ESC>');

visual_block('p', '"_dP')
normal("<leader>y", [["+y]])
visual("<leader>y", [["+y]])
normal("<leader>Y", [["+Y]])

normal("è", "<Cmd>wa<CR>")
normal("<BS>", "<Cmd>noh<CR>")
normal("ç", "<Cmd>vsplit<CR>")
normal("Ç", "<Cmd>split<CR>")

normal("<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

n_command('<F2>', vim.diagnostic.goto_next, 'Go to next diagnostic');
n_command('<F3>', vim.diagnostic.goto_prev, 'Go to previous diagnostic');

vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

normal(',', ';')
normal(';', ',')
