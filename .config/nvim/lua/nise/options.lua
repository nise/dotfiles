local opt = vim.opt
local indentation = 2

opt.autoindent = true
opt.expandtab = true
opt.tabstop = indentation
opt.softtabstop = indentation
opt.shiftwidth = indentation
opt.smartindent = true
opt.shiftround = true

opt.hidden = true
opt.number = true
opt.relativenumber = true
opt.mouse = "a"
opt.termguicolors = true
opt.cursorline = true
opt.cmdheight = 1
opt.fileencoding = "utf-8"
opt.hlsearch = true
opt.ignorecase = true
opt.pumheight = 10
opt.smartcase = true
opt.splitbelow = true
opt.splitright = true
opt.timeoutlen = 500
opt.undofile = true
opt.updatetime = 100
opt.writebackup = false
opt.backup = false
opt.swapfile = false
opt.laststatus = 3
opt.showcmd = false
opt.scrolloff = 8
opt.sidescrolloff = 8
opt.guifont = "monospace:h17"
opt.ruler = true
opt.completeopt = { "menuone", "noinsert", "noselect" }
opt.shortmess:append "c"

opt.linebreak = true
opt.breakindent = true

require 'colorizer'.setup { '*'; }

vim.cmd [[
  augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Visual', timeout = 200})
  augroup END
]]

