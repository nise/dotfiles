local nvim_data_path = vim.fn.stdpath("data")
local debugpy_runtime_path = nvim_data_path .. "/mason/packages/debugpy/venv/bin/python"
require('dap-python').setup(debugpy_runtime_path)
