-- If you started neovim within `~/dev/xy/project-1` this would resolve to `project-1`
local indentation = 4
vim.opt_local.tabstop = indentation
vim.opt_local.softtabstop = indentation
vim.opt_local.shiftwidth = indentation
local jdtls = require('jdtls')
local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')
local nvim_data_path = vim.fn.stdpath("data")
local mason_package_path = nvim_data_path .. "/mason/packages"
local bundles = { vim.fn.glob(mason_package_path ..
  "/java-debug-adapter/extension/server/com.microsoft.java.debug.plugin-*.jar"
  , true)
}

local home = os.getenv("HOME")
local workspace_dir = home .. '/workspaces/' .. project_name
vim.list_extend(bundles,
  vim.split(vim.fn.glob(mason_package_path .. "/java-test/extension/server/*.jar", true), "\n"))

local config = {
  java = {
    format = {
      enabled = true,
      settings = {
        url = '/home/nicolas/.config/java-formatter-settings.xml',
        profile = "GoogleStyle",
      }
    },
  },

  on_attach = function(client, bufnr)
    jdtls.setup_dap({ hotcodereplace = 'auto' })
    jdtls.setup.add_commands()
  end,
  cmd = { mason_package_path .. '/jdtls/bin/jdtls', '-data', workspace_dir },
  root_dir = vim.fs.dirname(vim.fs.find({ 'gradlew', '.git', 'mvnw' }, { upward = true })[1]),
  init_options = {
    bundles = bundles
  },
}

local server_configs = require('nise.java_configs').configs
local dap = require('dap')
dap.configurations.java = server_configs

jdtls.start_or_attach(config)
