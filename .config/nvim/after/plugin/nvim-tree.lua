require('nvim-tree').setup({
  view = {
    width = 45
  },
  renderer = {
    icons = {
      glyphs = {
        git = {
          unstaged = "",
          untracked = "",
          staged = "󰱒"
        }
      }
    }
  }
})
