require('neotest').setup {
  adapters = {
    require("neotest-python")({
      pytest_discover_instances = true
    })
  }
}
