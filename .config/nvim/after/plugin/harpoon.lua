local harpoon = require('harpoon')
harpoon:setup()
local which_key = require("which-key")

which_key.add({
  {
    '<leader>h',
    icon = '⇁',
    group = 'harpoon',
    { "<leader>a", function() harpoon:list():add() end, desc = 'Add file to harpoon list' },
    {
      { '<leader>hm', function() harpoon.ui:toggle_quick_menu(harpoon:list()) end, desc = 'Show harpoon menu' },
      { '<leader>hn', function() harpoon:list():next() end,                        desc = 'Navigate to next harpoon' },
      { '<leader>hp', function() harpoon:list():prev() end,                        desc = 'Navigate to previous harpoon' },
      { '<leader>ha', function() harpoon:list():select(1) end,                     desc = 'Navigate to harpoon #1' },
      { '<leader>hs', function() harpoon:list():select(2) end,                     desc = 'Navigate to harpoon #2' },
      { '<leader>hu', function() harpoon:list():select(2) end,                     desc = 'Navigate to harpoon #2' },
      { '<leader>hd', function() harpoon:list():select(3) end,                     desc = 'Navigate to harpoon #3' },
      { '<leader>hi', function() harpoon:list():select(3) end,                     desc = 'Navigate to harpoon #3' },
      { '<leader>hf', function() harpoon:list():select(4) end,                     desc = 'Navigate to harpoon #4' },
      { '<leader>he', function() harpoon:list():select(4) end,                     desc = 'Navigate to harpoon #4' },
      { '<leader>hg', function() harpoon:list():select(4) end,                     desc = 'Navigate to harpoon #4' },
    },
  }
})
