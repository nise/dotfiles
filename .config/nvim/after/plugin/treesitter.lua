require('tree-sitter-just').setup {}

require 'nvim-treesitter.configs'.setup {
  ensure_installed = { "elixir", "heex", "eex", "html", "surface", "rust", "lua", "markdown", "markdown_inline", "javascript", "typescript",
    "tsx", "css", "yaml", "java", "bash", "json", "python", "arduino", "c", "cpp", "dockerfile", "kotlin", "c_sharp", "nix", "just", "jq", "csv" },
  sync_install = false,
  ignore_install = {},
  highlight = {
    enable = true,
    disable = {}
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-h>',
      node_incremental = '<c-h>',
      node_decremental = '<M-h>',
      scope_incremental = '<S-h>',
    }
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          [']m'] = '@function.outer',
          [']['] = '@class.outer',
        },
        goto_next_end = {
          [']M'] = '@function.outer',
          [']]'] = '@class.outer',
        },
        goto_previous_start = {
          ['[m'] = '@function.outer',
          ['[['] = '@class.outer',
        },
        goto_previous_end = {
          ['[M'] = '@function.outer',
          ['[]'] = '@class.outer',
        },
      },
      swap = {
        enable = true,
        swap_next = {
          ['<leader>a'] = '@parameter.inner',
        },
        swap_previous = {
          ['<leader>A'] = '@parameter.inner',
        },
      }
    }
  }
}

require('nvim-ts-autotag').setup()
