local telescope = require('telescope')
telescope.load_extension("live_grep_args")
local lga_actions = require('telescope-live-grep-args.actions')
telescope.setup {
  extensions = {
    live_grep_args = {
      auto_quoting = true, -- enable/disable auto-quoting
      -- define mappings, e.g.
      mappings = {         -- extend mappings
        i = {
          ["<C-k>"] = lga_actions.quote_prompt(),
        }
      }
    },
    ["ui-select"] = {
      require('telescope.themes').get_dropdown {}
    }
  },
  defaults = {
    path_display = { shorten = { len = 2, exclude = { 1, -1, -2 } } }
  }
}
telescope.load_extension("live_grep_args")
telescope.load_extension('fzf')
telescope.load_extension("ui-select")

local which_key = require("which-key")
local opts = {
  mode = "n",
  prefix = "<leader>",
  buffer = nil,
  silent = true,
  noremap = true,
  nowait = true
}
local file_picker_layout = { preview_height = 0, prompt_position = 'top' }
local telescope_builtins = require('telescope.builtin')

-- TODO:
-- builtin.command_history
-- builtin.search_history
-- builtin.quickfixlist
-- builtin.registers
-- builtin.lsp_outgoing_calls
-- builtin.lsp_type_definitions
-- builtin.git_bcommits_range

which_key.add({
  {
    '<leader>l',
    group = 'LSP',
    icon = '󱃖',
    {
      {
        "<leader>lr",
        function()
          telescope_builtins.lsp_references({
            layout_strategy = 'vertical'
          })
        end,
        desc = "Find references"
      },
      { "<leader>li", telescope_builtins.lsp_implementations,  desc = "List implementations" },
      { "<leader>ld", telescope_builtins.lsp_definitions,      desc = "Go to definition" },
      { "<leader>lc", telescope_builtins.lsp_incoming_calls,   desc = "List incoming calls" },
      { "<leader>ls", telescope_builtins.lsp_document_symbols, desc = "List document symbols" },
      { "<leader>lv", telescope_builtins.diagnostics,          desc = "Show diagnostics" },
    },
  },
  {
    '<leader>g',
    group = 'Git',
    {
      "<leader>gh",
      function()
        telescope_builtins.git_bcommits({
          git_command = { "gitlog.sh" },
        })
      end,
      desc = "Show current buffer history"
    },
    { "<leader>gH", telescope_builtins.git_commits, desc = "Show workdir history" },
  },
  {
    '<leader>f',
    -- icon = '⇁',
    group = 'find',
    {
      "<leader>ff",
      function()
        telescope_builtins.git_files({
          layout_strategy = 'vertical',
          layout_config = file_picker_layout,
          sorting_strategy = 'ascending'
        })
      end,
      desc = "Find in git files"
    },
    {
      "<leader>fF",
      function()
        telescope_builtins.find_files({
          hidden = true,
          no_ignore = true,
          layout_strategy = 'vertical',
          layout_config = file_picker_layout,
          sorting_strategy = 'ascending'
        })
      end,
      desc = "Find in all files"
    },{
      "<leader>fç",
      function()
        telescope_builtins.find_files({
          hidden = true,
          no_ignore = true,
          layout_strategy = 'vertical',
          layout_config = file_picker_layout,
          sorting_strategy = 'ascending'
        })
      end,
      desc = "Find in all files"
    },
    { "<leader>fo", telescope_builtins.oldfiles,  desc = "Open old files" },

    {
      "<leader>fg",
      function()
        telescope.extensions.live_grep_args.live_grep_args({ layout_strategy = 'vertical', })
      end,
      desc = "Find text"
    },
    {
      "<leader>fG",
      function()
        telescope_builtins.live_grep({
          layout_strategy = 'vertical',
          additional_args = { '--no-ignore', '--hidden' }
        })
      end,
      desc = "Find text everywhere"
    },

    {
      "<leader>fc", telescope_builtins.grep_string, desc = "Find text under cursor" },
    {
      "<leader>fb",
      function()
        telescope_builtins.buffers({ path_display = { 'truncate' } })
      end,
      desc = "Find buffer"
    },
    {
      "<leader>fh", telescope_builtins.help_tags, desc = "Find help tag" },
    {
      "<leader>f/",
      function()
        telescope_builtins.current_buffer_fuzzy_find({
          sorting_strategy = 'ascending',
          layout_config = { prompt_position = 'top' }
        })
      end,
      desc = "Find in current buffer"
    },
    { "<leader>fm", telescope_builtins.man_pages, desc = "Find man page" },
    { "<leader>r",  telescope_builtins.resume,    desc = "Resume telescope" },
  }
})
vim.cmd("autocmd User TelescopePreviewerLoaded setlocal number")
