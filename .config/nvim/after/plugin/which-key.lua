local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
  return
end

local dap = require('dap')
local dap_ui = require('dapui')
local jdtls = require('jdtls')
local zen = require('zen-mode')
local buffer = vim.lsp.buf
local gs = require("gitsigns")
local mason_ui = require('mason.ui')
local baleia = require('baleia').setup {}
local theme = require("nise.theme")
local neotest = require("neotest")

which_key.setup {
  icons = {
    group = "󰙅 "
  }
}

which_key.add({
  { "<leader>c", theme.toggle,           desc = "Toggle between light and dark themes" },
  { "<leader>z", zen.toggle,             desc = "Toggle zen mode" },
  { "<leader>u", vim.cmd.UndotreeToggle, desc = "Toggle undo tree" },
  {
    '<leader>g',
    group = 'Git',
    {
      { "<leader>gd", gs.preview_hunk,              desc = "Show inline diff" },
      { "<leader>gu", gs.reset_hunk,                desc = "Undo hunk" },
      { "<leader>gD", gs.diffthis,                  desc = "Show file diff" },
      { "<leader>gj", gs.next_hunk,                 desc = "Go to next changed hunk" },
      { "<leader>gk", gs.prev_hunk,                 desc = "Go to previous changed hunk" },
      { "<leader>gt", gs.next_hunk,                 desc = "Go to next changed hunk" },
      { "<leader>gs", gs.prev_hunk,                 desc = "Go to previous changed hunk" },
      { "<leader>gb", gs.toggle_current_line_blame, desc = "Toggle blame for current line" },
      {
        "<leader>gB",
        function()
          gs.blame_line { full = true }
        end,
        desc = "Blame current line"
      },
    }
  },
  {
    '<leader>l',
    group = 'LSP',
    icon = '󱃖',
    {
      { "<leader>la", buffer.code_action,      desc = "Code Action",              mode = { "n", "v" } },
      { "<leader>ln", buffer.rename,           desc = "Rename symbol" },
      { "<leader>lh", buffer.hover,            desc = "Show documentation" },
      { "<leader>lS", buffer.signature_help,   desc = "Show signature help" },
      { "<leader>lw", buffer.workspace_symbol, desc = "List workspace symbols" },
      { "<leader>ll", mason_ui.open,           desc = "Open LSP installer window" },
      {
        "<leader>lf",
        function()
          buffer.format { async = true }
        end,
        desc = "Reformat code",
        mode = { "n", "v" }
      },

    }
  },
  {
    '<leader>t',
    group = 'tree',
    {
      { "<leader>tt", vim.cmd.NvimTreeToggle,         desc = "Toggle file tree" },
      { "<leader>tf", vim.cmd.NvimTreeFindFileToggle, desc = "Find file in tree" },
      { "<leader>tn", vim.cmd.NnnExplorer,            desc = "Open nnn" }
    }
  },
  {
    '<leader>d',
    group = 'Debug',
    {
      { "<F7>",  dap.step_into, desc = "Step into" },
      { "<F8>",  dap.step_over, desc = "Step over" },
      { "<F9>",  dap.continue,  desc = "Continue" },
      { "<F10>", dap.terminate, desc = "Terminate" },
      { "<F11>", dap.step_out,  desc = "Step out" },
      {
        "<leader>dm",
        function()
          if vim.bo.filetype == 'java' then
            jdtls.test_nearest_method()
            dap.repl.open({ width = 60 }, 'vsplit')
          else
            neotest.run.run({ strategy = "dap" })
            neotest.summary.open()
          end
        end,
        desc = "Test nearest method"
      },
      {
        "<leader>dc",
        function()
          if vim.bo.filetype == 'java' then
            jdtls.test_class()
            dap.repl.open({ width = 60 }, 'vsplit')
          else
            neotest.run.run(vim.fn.expand('%'))
            neotest.summary.open()
          end
        end,
        desc = "Test class"
      },
      { "<leader>db", dap.toggle_breakpoint, desc = "Toggle breakpoint" },
      { "<leader>dr", dap.repl.open,         desc = "Open repl" },
      { "<leader>df", dap.restart_frame,     desc = "Restart frame" },
      {
        "<leader>dv",
        function()
          dap_ui.toggle({ layout = 1 })
        end,
        desc = "Toggle dap ui"
      },
      {
        "<leader>dd",
        function()
          dap_ui.toggle({ layout = 3 })
        end,
        desc = "Toggle dap ui"
      },
      {
        "<leader>dl",
        function()
          dap_ui.toggle({ layout = 2 })
        end,
        desc = "Show terminal"
      },
      { "<leader>de", dap_ui.eval, desc = "Evaluate selected expression", mode = "v" }
    }
  },
  { "<leader>C", function() baleia.once(vim.fn.bufnr('%')) end, desc = "Parse ANSI escape sequences" },
})
