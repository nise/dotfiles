require("dapui").setup({
  layouts = {
    {
      elements = {
        { id = "scopes",      size = 0.50 },
        { id = "breakpoints", size = 0.25 },
        { id = "stacks",      size = 0.25 },
      },
      position = "left",
      size = 80
    },
    {
      elements = {
        { id = "repl",    size = 0.2 },
        { id = "console", size = 0.8 }
      },
      position = "bottom",
      size = 60
    },
    {
      elements = {
        { id = "watches", size = 1 },
      },
      position = "bottom",
      size = 16
    }
  },
})

local dap = require('dap')

local nvim_data_path = vim.fn.stdpath("data")
local mason_package_path = nvim_data_path .. "/mason/packages"
dap.adapters.mix_task = {
  type = 'executable',
  command = mason_package_path .. '/elixir-ls/debugger.sh',
  args = {}
}

dap.configurations.elixir = {
  {
    type = "mix_task",
    name = "run phoenix server",
    request = "launch",
    task = "phx.server",
    projectDir = "${workspaceFolder}",
  }
}
