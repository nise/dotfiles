local servers = require("nise.lspservers").servers

require("mason").setup()
require("mason-lspconfig").setup({
  automatic_installation = { exclude = { "rust_analyzer" } },
  ensure_installed = servers
})
local nvim_data_path = vim.fn.stdpath("data")
local mason_package_path = nvim_data_path .. "/mason/packages"

vim.diagnostic.config({
  update_in_insert = true,
  float = {
    header = ""
  }
})

local nvim_lsp = require 'lspconfig'

require("neodev").setup {
  library = { plugins = { "neotest" }, types = true },
}
nvim_lsp.lua_ls.setup {}

nvim_lsp.jedi_language_server.setup {}
nvim_lsp.pyright.setup {
  settings = {
    python = {
      analysis = {
        diagnosticMode = "workspace"
      }
    }
  }
}
nvim_lsp.ruff.setup {}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

nvim_lsp.elixirls.setup {
  cmd = { mason_package_path .. "/elixir-ls/language_server.sh" },
  capabilities = capabilities,
}

capabilities.textDocument.completion.completionItem.snippetSupport = true
nvim_lsp.html.setup {
  capabilities = capabilities,
  filetypes = { "html", "heex" }
}

nvim_lsp.jsonls.setup {
  capabilities = capabilities
}

nvim_lsp.yamlls.setup {
  settings = {
    yaml = {
      keyOrdering = false
    }
  }
}

nvim_lsp.ts_ls.setup {}

nvim_lsp.angularls.setup {}

nvim_lsp.dockerls.setup {}

nvim_lsp.taplo.setup {}
