local zen = require('zen-mode')
zen.setup{
  width = 60
}

vim.api.nvim_create_autocmd({"BufEnter"}, {
  pattern = {"*.md"},
  callback = zen.toggle
})
