#!/usr/bin/env bash

git log --date="format:%y/%m/%d" --pretty="format:%h %ad %an | %s" --follow $1 \
  >/tmp/gitlog \
  && echo "" >> /tmp/gitlog \
  && cat /tmp/gitlog
